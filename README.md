### Introduction ###

Thank you for taking the time to do this test. This little challenge will give you a good idea of what will be your daily job in a bigger scale. The result of this test will serve as a base for the next part of the interview. Happy programming!


### Instructions ###

* Clone this repo locally.
* Look at this design file: design/Design VueJs Exam.xd. You will need Adobe XD to read this file.
* Go into the app folder and install the Vue app using the command "npm install".
* Run the app using the command "npm run serve". If all goes well then you should be able to see the app running at http://localhost:8080/
* Implement the design in this Vue.js app. In short, you need to implement the design and the functionalities for the product list, the product details page and the form.
* Use the json data from this file for the products: app/src/assets/products.json.
* Once you are done, push your code into this git repo.
* You have maximum 1 day (24h) to do this exam and deliver your code.


### You will be evaluated on the following criterias ###

* Following the design and building the UI in pixel perfect mode. It means respecting the font size, spacing, color and so on.
* Using and structuring your Vue.js code in components.
* Using Vue Router to navigate between the list and the details/form. The Vue Router module is already installed in this app.
* Using Vuex to store, get and update the products. The Vuex module is already installed in this app.
* Having clear and clean code.
* Completing the project.